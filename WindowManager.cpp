//
// Created by serj on 05.01.23.
//

#include "WindowManager.h"

#include <iostream>
#include <cstring>
#include <unistd.h>
#include <pwd.h>
#include <cstdio>
#include <cwchar>
#include <cstdlib>
#include <unistd.h>
#include <sys/time.h>
#include "csignal"
#include <memory>
#include <array>
#include <fstream>

void CreateWindow(int wdt, int hgt){

    dpy = XOpenDisplay(NULL);

    if(dpy == NULL) {
        printf("\n\tcannot connect to X server\n\n");
        exit(0);
    }

    root = DefaultRootWindow(dpy);

    vi = glXChooseVisual(dpy, 0, att);
    XMatchVisualInfo(dpy, DefaultScreen(dpy), 32, TrueColor, vi);

    if(vi == NULL) {
        printf("\n\tno appropriate visual found\n\n");
        exit(0);
    }
    else {
        printf("\n\tvisual %p selected\n", (void *)vi->visualid); //  %p creates hexadecimal output like in glxinfo * /
    }


    swa.colormap = XCreateColormap(dpy, root, vi->visual, AllocNone);
    //swa.event_mask = ExposureMask | StructureNotifyMask |  CWOverrideRedirect | PropertyChangeMask |
    //                 SubstructureRedirectMask ; //KeyPressMask; ResizeRedirectMask

    swa.event_mask = ExposureMask | KeyPressMask; // ResizeRedirectMask

    swa.border_pixel = 10;
    swa.background_pixel =  0xffffffff; //0; // 0x80808080;
//    swa.override_redirect = 1; // disable mouse but make window appear over others

    /* // borrowed from conky:

     XSetWindowAttributes attrs = {ParentRelative,
                                   0L,
                                   0,
                                   0L,
                                   0,
                                   0,
                                   Always,
                                   0L,
                                   0L,
                                   False,
                                   StructureNotifyMask | ExposureMask,
                                   0L,
                                   True,
                                   0,
                                   0};
     //attrs.colormap = win.colourmap;
     //flags |= CWBorderPixel | CWColormap;

     //XResizeWindow
     //set_transparent_background();
     //get_x11_desktop_info(dpy, 0);
     //XSetWindowBackground(reinterpret_cast<Display*>(dpy), win, 0); // ogl x */


    win = XCreateWindow(dpy, root, 0, 0, wdt, hgt, 0, vi->depth, InputOutput, vi->visual, CWColormap | CWEventMask |
                                                                                          CWBorderPixel | CWBackPixel | CWOverrideRedirect | PropertyChangeMask | SubstructureRedirectMask |
                                                                                          SubstructureNotifyMask, &swa);

    // disable mouse events
    XRectangle rect;
    XserverRegion region = XFixesCreateRegion(dpy, &rect, 1);
    XFixesSetWindowShapeRegion(dpy, win, ShapeInput, 0, 0, region);
    XFixesDestroyRegion(dpy, region);


    //XLowerWindow(dpy, win);
    //XSetClassHint(dpy, win, &classHint);

//    auto xa = XInternAtom(dpy, "_WIN_LAYER", False); //ATOM(_WIN_LAYER);
//    if (xa != None) {
//        long prop = 0;
//
//        XChangeProperty(dpy, win, xa, XA_CARDINAL, 32,
//                        PropModeAppend,
//                        reinterpret_cast<unsigned char *>(&prop), 1);
//    }
//
//    xa = XInternAtom(dpy, "_NET_WM_STATE", False); //ATOM(_NET_WM_STATE);
//    if (xa != None) {
//        Atom xa_prop = XInternAtom(dpy, "_NET_WM_STATE_BELOW", False); //ATOM(_NET_WM_STATE_BELOW);
//
//        XChangeProperty(dpy, win, xa, XA_ATOM, 32,
//                        PropModeAppend,
//                        reinterpret_cast<unsigned char *>(&xa_prop), 1);
//    }


//    Atom window_type = XInternAtom(dpy, "_NET_WM_WINDOW_TYPE", False);
//    Atom dock = XInternAtom(dpy, "_NET_WM_WINDOW_TYPE_DOCK", False);
//    XChangeProperty (dpy, win, window_type, XA_ATOM, 32, PropModeReplace, (unsigned char *) &dock, 1);
//
//    //---
//    //XLowerWindow(dpy, win);
//    // should send window to background, but it didnt work
//    //Atom window_type = XInternAtom(dpy, "_NET_WM_WINDOW_TYPE", False);
//    Atom desktop = XInternAtom(dpy, "_NET_WM_WINDOW_TYPE_DESKTOP", False);
//    XChangeProperty (dpy, win, window_type, XA_ATOM, 32, PropModeReplace, (unsigned char *) &desktop, 1);
//    //----------
//
//    // borrowed from here https://github.com/ujjwal96/xwinwrap
//    Atom window_layer = XInternAtom(dpy, "_WIN_LAYER", False);
//    XChangeProperty (dpy, win, window_type, XA_CARDINAL, 32, PropModeReplace, (unsigned char *) &window_layer, 1);
//    Atom below_desktop = XInternAtom(dpy, "_NET_WM_STATE_BELOW", False);
//    XChangeProperty (dpy, win, window_type, XA_ATOM, 32, PropModeReplace, (unsigned char *) &below_desktop, 1);
//
//    Atom window_taskbar = XInternAtom(dpy, "_NET_WM_STATE_SKIP_TASKBAR", False);
//    XChangeProperty(dpy, win, window_type, XA_ATOM, 32, PropModeAppend, (unsigned char *) &window_taskbar, 1);
//
//    Atom window_pager = XInternAtom(dpy, "_NET_WM_STATE_SKIP_PAGER", False);
//    XChangeProperty(dpy, win, window_type, XA_ATOM, 32, PropModeAppend, (unsigned char *) &window_pager, 1);
    //--------

    //MakeAlwaysOnTop(dpy, root, win);

    XMapWindow(dpy, win);
    XStoreName(dpy, win, "VERY SIMPLE APPLICATION");
}