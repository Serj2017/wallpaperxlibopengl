
#include <pwd.h>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include "WindowManager.h"
#include "OGLProcessing.h"

#include "common/shader.hpp"

//  https://github.com/panluDreamer/ShaderToy-OpenGL-Framework.git
//#include "ShadeTroyShaders/Shader.h"
//#include "ShadeTroyShaders/Model.h"

// https://physicspython.wordpress.com/2020/03/04/visualizing-the-mandelbrot-set-using-opengl-part-2/
glm::vec4 find_ranges(std::vector<float> & data)
{
    std::sort(data.begin(), data.end());
    int lowest = 0;
    while (data[lowest] == 0.0f)
    {
        ++lowest;
        }

    int size = data.size();
    int length = size - lowest;
    glm::vec4 ranges = glm::vec4( data[lowest], data[lowest + length * 3 / 4 - 1], data[lowest + length * 7 / 8 - 1], data[size - 1] );
    return ranges;
}

//int wdt = 800, hgt = 800;
int wdt = 1920, hgt = 1080;

int main(int argc, char *argv[])
{
    dpy = XOpenDisplay(NULL);

    if(dpy == NULL) {
        printf("\n\tcannot connect to X server\n\n");
        exit(0);
    }

    root = DefaultRootWindow(dpy);

    vi = glXChooseVisual(dpy, 0, att);
    XMatchVisualInfo(dpy, DefaultScreen(dpy), 32, TrueColor, vi);

    if(vi == NULL) {
        printf("\n\tno appropriate visual found\n\n");
        exit(0);
    }
    else {
        printf("\n\tvisual %p selected\n", (void *)vi->visualid); //  %p creates hexadecimal output like in glxinfo * /
    }

    swa.colormap = XCreateColormap(dpy, root, vi->visual, AllocNone);
    swa.event_mask = ExposureMask | KeyPressMask;
    swa.border_pixel = 10;
    swa.background_pixel =  None;

    win = XCreateWindow(dpy, root, 0, 0, wdt, hgt, 0, vi->depth, InputOutput, vi->visual, CWColormap | CWEventMask |
                                                                                          CWBorderPixel | CWBackPixel | CWOverrideRedirect | PropertyChangeMask | SubstructureRedirectMask |
                                                                                          SubstructureNotifyMask, &swa);
    Atom window_type = XInternAtom(dpy, "_NET_WM_WINDOW_TYPE", False);
    Atom desktop = XInternAtom(dpy, "_NET_WM_WINDOW_TYPE_DESKTOP", False);
    XChangeProperty (dpy, win, window_type, XA_ATOM, 32, PropModeReplace, (unsigned char *) &desktop, 1);

    // disable mouse events
    XRectangle rect;
    XserverRegion region = XFixesCreateRegion(dpy, &rect, 1);
    XFixesSetWindowShapeRegion(dpy, win, ShapeInput, 0, 0, region);
    XFixesDestroyRegion(dpy, region);

    XMapWindow(dpy, win);

    glc = glXCreateContext(dpy, vi, NULL, GL_TRUE);
    glXMakeCurrent(dpy, win, glc);
    glEnable(GL_DEPTH_TEST);


    //--- ogl
    glewExperimental = true; // Needed for core profile
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        getchar();
        glfwTerminate();
        return -1;
    }

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    glClearColor(0.0f, 0.0f, 0.0f, 0.5f);

    // https://r105.threejsfundamentals.org/threejs/lessons/threejs-shadertoy.html
//    Model quad;
//    quad.init("../ShadeTroyShaders/quad.obj", false, true, false, false);
//    Shader shader;
//    //shader.init("shader/main_vert.glsl", "shader/fire_ball_frag.glsl");
//    //shader.init("../ShadeTroyShaders/main_vert.glsl", "../ShadeTroyShaders/unreal_intro_frag.glsl");
//    shader.init("../ShadeTroyShaders/main_vert.glsl", "../ShadeTroyShaders/fire_ball_frag.glsl");
//    vec3 iResolution = vec3(800, 600, 0);
//    clock_t start_time = clock();
//    clock_t curr_time;
//    float playtime_in_second = 0;

/*
    GLuint VertexArrayID;
    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);

    // https://softologyblog.wordpress.com/category/glsl/
    // Create and compile our GLSL program from the shaders
    ///*
    GLuint programID = LoadShaders( "../shaders/TransformVertexShader.vertexshader",
                                    "../shaders/ColorFragmentShader.fragmentshader" );

    GLuint MatrixID = glGetUniformLocation(programID, "MVP");

    glm::mat4 Projection = glm::perspective(glm::radians(45.0f), 4.0f / 3.0f, 0.1f, 100.0f);
    glm::mat4 View       = glm::lookAt(
            glm::vec3(4,3,-3),
            glm::vec3(0,0,0),
            glm::vec3(0,1,0)
    );

    glm::mat4 Model      = glm::mat4(1.0f);
    glm::mat4 MVP        = Projection * View * Model; 

    GLuint vertexbuffer;
    glGenBuffers(1, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

    GLuint colorbuffer;
    glGenBuffers(1, &colorbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(g_color_buffer_data), g_color_buffer_data, GL_STATIC_DRAW);
*/


    //glViewport(0, 0, wdt, hgt);

    //XGrabPointer(dpy, win, True, ButtonPressMask, GrabModeAsync, // disable mouse
    //             GrabModeAsync, win, None, CurrentTime);


    GLuint programID = LoadShaders( "../shaders/shader.vert",
                                    "../shaders/shader.frag" );

    unsigned int VAO, VBO, EBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    last_time = glfwGetTime();

    glEnable(GL_DEPTH_TEST);

    std::vector<float> pixel_data(wdt * hgt, 0.0f);
    glm::vec4 ranges = glm::vec4(0.0001f, 0.33333f, 0.66667f, 1.00f);

    while(1) {
        XEvent event;
        if (XPending(dpy)){
            XNextEvent(dpy, &event);
            // X Event Handler
            if(xev.type == Expose) {
            }
        else if(xev.type == KeyPress) {
            glXMakeCurrent(dpy, None, NULL);
            glXDestroyContext(dpy, glc);
            XDestroyWindow(dpy, win);
            XCloseDisplay(dpy);
            exit(0);
            }
        }

        /*glClear(GL_COLOR_BUFFER_BIT);
        curr_time = clock();
        playtime_in_second = (curr_time - start_time)*1.0f / 1000.0f;
        cout << "playtime_in_second = " << playtime_in_second << endl;
        shader.use();
        shader.bind_vec3("iResolution", iResolution);
        shader.bind_float("iTime",playtime_in_second);
        quad.render();*/

/*
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glUseProgram(programID);
        glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

        // 1rst attribute buffer : vertices
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
        glVertexAttribPointer(
                0,                  // attribute. No particular reason for 0, but must match the layout in the shader.
                3,                  // size
                GL_FLOAT,           // type
                GL_FALSE,           // normalized?
                0,                  // stride
                (void*)0            // array buffer offset
        );

        // 2nd attribute buffer : colors
        glEnableVertexAttribArray(1);
        glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
        glVertexAttribPointer(
                1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
                3,                                // size
                GL_FLOAT,                         // type
                GL_FALSE,                         // normalized?
                0,                                // stride
                (void*)0                          // array buffer offset
        );

        glDrawArrays(GL_TRIANGLES, 0, 12*3); // 12*3 indices starting at 0 -> 12 triangles

        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
*/

        glClearColor(0.2f, 0.0f, 0.2f, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glUseProgram(programID);
        glUniform1f(glGetUniformLocation(programID, "zoom"), zoom);
        glUniform1f(glGetUniformLocation(programID, "center_x"), center_x);
        glUniform1f(glGetUniformLocation(programID, "center_y"), center_y);
        glUniform4f(glGetUniformLocation(programID, "color_ranges"), ranges.x, ranges.y, ranges.z, ranges.w);

        glBindVertexArray(VAO);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

        glReadPixels(0, 0, wdt, hgt, GL_DEPTH_COMPONENT, GL_FLOAT, pixel_data.data());
        ranges = find_ranges(pixel_data);

        glXSwapBuffers(dpy, win);

    }
}